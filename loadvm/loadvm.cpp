// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "kvmmm.hpp"

#include <cstdio>
#include <cstdlib>
#include <optional>
#include <string>
#include <vector>

constexpr size_t ram_size = 2ULL << 32;
constexpr unsigned short stdio_port = 0x3F8;

struct Image {
    std::string filename {};
    std::optional<uint64_t> location {};
    std::optional<uint64_t> start {};
    std::optional<uint64_t> size {};
};

struct Arguments {
    int verbosity {};
    std::optional<uint64_t> entry {};
    std::optional<std::string> arch {};
    std::optional<std::string> elf {};
    std::vector<Image> images {};
};

static void usage(FILE* file, int exitcode) {
    ::fprintf(file, "Usage: loadvm [OPTION...]\n\n");
    ::fprintf(file, "Load data to run with KVM\n\n");
    ::fprintf(file, "Options:\n");
    ::fprintf(file, "  -h, --help            Display this help\n");
    ::fprintf(file, "  -v, --verbose         Increase verbosity\n");
    ::fprintf(
            file,
            "  -e ADDRESS, --entry ADDRESS\n"
            "                        Set entry point for execution\n");
    ::fprintf(
            file,
            "  -a ARCHITECTURE, --arch ARCHITECTURE\n"
            "                        Set architecture\n");
    ::fprintf(file, "  --elf FILE            Load static executable\n");
    ::fprintf(
            file,
            "  --image FILE[:MEMORY_OFFSET[:FILE_OFFSET[:FILE_LENGTH]]]\n"
            "                        Load code image into memory\n");

    ::exit(exitcode);
}

static Image parse_arguments_image(char* string) {
    Image image {};

    auto extract_string = [&]() {
        std::string retval {};

        while (*string != '\0' && *string != ':') {
            retval += *string;
            string += 1;
        }

        if (*string == ':') {
            string += 1;
        }

        return retval;
    };

    auto extract_uint64 = [&]() {
        std::string value = extract_string();
        if (value.empty()) {
            usage(stderr, 1);
        }

        return std::stoull(value, nullptr, 0);
    };

    image.filename = extract_string();

    if (*string) {
        image.location = extract_uint64();
        if (*string) {
            image.start = extract_uint64();
            if (*string) {
                image.size = extract_uint64();
                if (*string) {
                    usage(stderr, 1);
                }
            }
        }
    }

    return image;
}

static Arguments parse_arguments(char* argv[]) {
    Arguments arguments {};

    while (*++argv) {
        std::string arg { *argv };
        if (arg == "--help" || arg == "-h") {
            usage(stdout, 0);
        } else if (arg == "--verbose" || arg == "-v") {
            arguments.verbosity += 1;
        } else if (arg == "--entry" || arg == "-e") {
            char* data = *++argv;
            if (!data || arguments.entry) {
                usage(stderr, 1);
            }
            arguments.entry = std::stoull(data, nullptr, 0);
        } else if (arg == "--arch" || arg == "-a") {
            char* data = *++argv;
            if (!data || arguments.arch) {
                usage(stderr, 1);
            }
            arguments.arch = data;
        } else if (arg == "--elf") {
            char* data = *++argv;
            if (!data || arguments.elf) {
                usage(stderr, 1);
            }
            arguments.elf = data;
        } else if (arg == "--image") {
            char* data = *++argv;
            if (!data) {
                usage(stderr, 1);
            }
            arguments.images.push_back(parse_arguments_image(data));
        } else {
            usage(stderr, 1);
        }
    }

    return arguments;
}

static void load_elf(kvmmm::Mem& ram, Arguments& arguments) {
    if (!arguments.elf) {
        return;
    }

    kvmmm::Mem elf = kvmmm::Mem::file(arguments.elf.value());
    auto magic = elf.as<uint32_t>(0x00);
    auto type = elf.as<uint16_t>(0x10);
    auto machine = elf.as<uint16_t>(0x12);

    if (magic != 0x464C457F) {
        fprintf(stderr, "error: invalid elf magic\n");
        exit(1);
    }
    if (type != 0x02) {
        fprintf(stderr, "error: ELF must be (static) executable\n");
        exit(1);
    }
    if (machine != 0x03 && machine != 0x3E) {
        fprintf(stderr, "error: ELF has incompatible ISA\n");
        exit(1);
    }

    auto format = elf.as<uint8_t>(0x04);

    if (arguments.verbosity > 0) {
        printf(
                "info: ELF format elf%s-%s\n",
                (format == 1) ? "32" : "64",
                (machine == 0x03) ? "i386" : "x86-64");
    }

    uint64_t entry;
    uint64_t phoff;
    uint16_t phentsize;
    uint16_t phnum;

    if (format == 1) {
        entry = elf.as<uint32_t>(0x18);
        phoff = elf.as<uint32_t>(0x1C);
        phentsize = elf.as<uint16_t>(0x2A);
        phnum = elf.as<uint16_t>(0x2C);
    } else {
        entry = elf.as<uint64_t>(0x18);
        phoff = elf.as<uint64_t>(0x20);
        phentsize = elf.as<uint16_t>(0x36);
        phnum = elf.as<uint16_t>(0x38);
    }

    if (!arguments.entry) {
        arguments.entry = entry;
    }

    if (!arguments.arch) {
        arguments.arch = (machine == 0x03) ? "i386" : "x86-64";
    }

    for (size_t i = 0; i < phnum; ++i) {
        uint64_t base = phoff + phentsize * i;
        if (elf.as<uint32_t>(base) != 0x01) {
            continue;
        }

        uint64_t offset;
        uint64_t vaddr;
        uint64_t filesz;
        uint64_t memsz;

        if (format == 1) {
            offset = elf.as<uint32_t>(base + 0x04);
            vaddr = elf.as<uint32_t>(base + 0x08);
            filesz = elf.as<uint32_t>(base + 0x10);
            memsz = elf.as<uint32_t>(base + 0x14);
        } else {
            offset = elf.as<uint64_t>(base + 0x08);
            vaddr = elf.as<uint64_t>(base + 0x10);
            filesz = elf.as<uint64_t>(base + 0x20);
            memsz = elf.as<uint64_t>(base + 0x28);
        }

        if (arguments.verbosity > 0) {
            printf(
                    "info: ELF segment %zu "
                    "file: 0x%lx (+ 0x%lx) mem: 0x%lx (+ 0x%lx)\n",
                    i,
                    offset,
                    filesz,
                    vaddr,
                    memsz);
        }

        for (uint64_t i = 0; i < std::min(filesz, memsz); ++i) {
            ram.ptr()[vaddr + i] = elf.ptr()[offset + i];
        }
        for (uint64_t i = filesz; i < memsz; ++i) {
            ram.ptr()[vaddr + i] = std::byte { 0 };
        }
    }
}

static void load_images(kvmmm::Mem& ram, const Arguments& arguments) {
    for (size_t i = 0; i < arguments.images.size(); ++i) {
        auto& image = arguments.images[i];
        kvmmm::Mem mem = kvmmm::Mem::file(image.filename);
        auto location = image.location.value_or(0);
        auto start = image.start.value_or(0);
        auto size = image.size.value_or(mem.size());

        if (arguments.verbosity > 0) {
            printf(
                    "info: Image %zu: 0x%lx (+ 0x%lx)\n",
                    i,
                    location,
                    size);
        }

        for (size_t j = 0; j < size; ++j) {
            ram.ptr()[location + j] = mem.ptr()[start + j];
        }
    }
}

static void dump_regs(const kvm_regs& data) {
    printf("rax:   0x%llx\n", data.rax);
    printf("rbx:   0x%llx\n", data.rbx);
    printf("rcx:   0x%llx\n", data.rcx);
    printf("rdx:   0x%llx\n", data.rdx);
    printf("rsi:   0x%llx\n", data.rsi);
    printf("rdi:   0x%llx\n", data.rdi);
    printf("rsp:   0x%llx\n", data.rsp);
    printf("rbp:   0x%llx\n", data.rbp);
    printf("r8:    0x%llx\n", data.r8);
    printf("r9:    0x%llx\n", data.r9);
    printf("r10:   0x%llx\n", data.r10);
    printf("r11:   0x%llx\n", data.r11);
    printf("r12:   0x%llx\n", data.r12);
    printf("r13:   0x%llx\n", data.r13);
    printf("r14:   0x%llx\n", data.r14);
    printf("r15:   0x%llx\n", data.r15);
    printf("rip:   0x%llx\n", data.rip);
    printf("flags: 0x%llx\n", data.rflags);
}

static void dump_sregs(const kvm_sregs& data) {
    auto dump_segment = [](const char* name, const kvm_segment& data) {
        printf(
                "%s "
                "base: 0x%llx "
                "lim: 0x%x "
                "sel: 0x%x "
                "type: 0x%x "
                "pres: 0x%x "
                "dpl: 0x%x "
                "db: 0x%x "
                "s: 0x%x "
                "l: 0x%x "
                "g: 0x%x "
                "avl: 0x%x\n",
                name,
                data.base,
                data.limit,
                data.selector,
                data.type,
                data.present,
                data.dpl,
                data.db,
                data.s,
                data.l,
                data.g,
                data.avl);
    };

    auto dump_dtable = [](const char* name, const kvm_dtable& data) {
        printf("%s base: 0x%llx lim: 0x%x\n", name, data.base, data.limit);
    };

    dump_segment("cs", data.cs);
    dump_segment("ds", data.ds);
    dump_segment("es", data.es);
    dump_segment("fs", data.fs);
    dump_segment("gs", data.gs);
    dump_segment("ss", data.ss);
    dump_segment("tr", data.tr);
    dump_segment("ldt", data.ldt);
    dump_dtable("gdt", data.gdt);
    dump_dtable("idt", data.idt);
    printf("cr0:   0x%llx\n", data.cr0);
    printf("cr2:   0x%llx\n", data.cr2);
    printf("cr3:   0x%llx\n", data.cr3);
    printf("cr4:   0x%llx\n", data.cr4);
    printf("cr8:   0x%llx\n", data.cr8);
    printf("efer:   0x%llx\n", data.efer);
    printf("apic:   0x%llx\n", data.apic_base);
    for (int i = 0; i < (KVM_NR_INTERRUPTS + 63) / 64; ++i) {
        printf("int[%i]:   0x%llx\n", i, data.interrupt_bitmap[i]);
    }
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        usage(stderr, 1);
    }

    auto arguments = parse_arguments(argv);
    if (arguments.images.empty() && !arguments.elf) {
        usage(stderr, 1);
    }

    kvmmm::Mem ram = kvmmm::Mem::memory(ram_size);
    load_elf(ram, arguments);
    load_images(ram, arguments);

    kvmmm::KVM kvm;

    kvmmm::VM vm = kvm.create_vm();
    vm.set_memory(0, 0, ram);

    kvmmm::CPU cpu = vm.create_cpu();

    kvmmm::Mem cpu_mmap = kvm.create_cpu_map(cpu);

    if (!arguments.entry) {
        arguments.entry = arguments.images[0].location;
    }

    if (arguments.verbosity > 0) {
        printf("info: Entry point 0x%lx\n", arguments.entry.value());
    }

    if (!arguments.arch) {
        fprintf(stderr, "error: No architecture specified\n");
        return 1;
    } else if (*arguments.arch == "x86-16" || *arguments.arch == "x86") {
        kvmmm::initialize_real_mode(cpu, *arguments.entry);
    } else if (*arguments.arch == "x86-32" || *arguments.arch == "i386") {
        kvmmm::initialize_protected_mode(cpu, *arguments.entry);
    } else if (*arguments.arch == "x86-64" || *arguments.arch == "amd64") {
        kvmmm::initialize_long_mode(cpu, ram, *arguments.entry);
    } else {
        fprintf(stderr, "error: Unknown architecture\n");
        return 1;
    }

    if (arguments.verbosity > 0) {
        printf("info: Architecture %s\n", arguments.arch->c_str());
    }

    for (kvm_run* run = (kvm_run*)cpu_mmap.ptr();;) {
        cpu.run();

        switch (run->exit_reason) {
        case KVM_EXIT_UNKNOWN:
            printf(
                    "KVM exit: Unknown reason 0x%llx\n",
                    run->hw.hardware_exit_reason);
            return 1;

        case KVM_EXIT_EXCEPTION:
            printf(
                    "KVM exit: Exception 0x%x, 0x%x\n",
                    run->ex.exception,
                    run->ex.error_code);
            return 1;

        case KVM_EXIT_IO:
            if (run->io.direction != 0 && run->io.port == stdio_port) {
                fwrite(
                        cpu_mmap.ptr() + run->io.data_offset,
                        run->io.size,
                        run->io.count,
                        stdout);
                fflush(stdout);
                continue;
            }

            printf(
                    "KVM exit: IO 0x%x, 0x%x, 0x%x, 0x%x, 0x%llx\n",
                    run->io.direction,
                    run->io.size,
                    run->io.port,
                    run->io.count,
                    run->io.data_offset);
            return 1;

        case KVM_EXIT_DEBUG:
            printf(
                    "KVM exit: Debug 0x%x, 0x%llx, 0x%llx, 0x%llx\n",
                    run->debug.arch.exception,
                    run->debug.arch.pc,
                    run->debug.arch.dr6,
                    run->debug.arch.dr7);
            if (arguments.verbosity > 0) {
                dump_regs(cpu.regs());
                dump_sregs(cpu.sregs());
            }
            continue;

        case KVM_EXIT_HLT:
            return cpu.regs().rax;

        case KVM_EXIT_MMIO:
            printf(
                    "KVM exit: MMIO 0x%llx,"
                    " 0x%02x%02x%02x%02x%02x%02x%02x%02x, 0x%x, 0x%x\n",
                    run->mmio.phys_addr,
                    run->mmio.data[0],
                    run->mmio.data[1],
                    run->mmio.data[2],
                    run->mmio.data[3],
                    run->mmio.data[4],
                    run->mmio.data[5],
                    run->mmio.data[6],
                    run->mmio.data[7],
                    run->mmio.len,
                    run->mmio.is_write);
            return 1;

        case KVM_EXIT_SHUTDOWN:
            if (arguments.verbosity > 0) {
                dump_regs(cpu.regs());
                dump_sregs(cpu.sregs());
            }
            return 0;

        case KVM_EXIT_FAIL_ENTRY:
            printf("KVM exit: Entry failure: 0x%llx, 0x%x\n",
                   run->fail_entry.hardware_entry_failure_reason,
                   run->fail_entry.cpu);
            return 1;

        case KVM_EXIT_INTR:
            printf("KVM exit: Interrupt\n");
            return 1;

        case KVM_EXIT_NMI:
            printf("KVM exit: NMI\n");
            return 1;

        case KVM_EXIT_INTERNAL_ERROR:
            printf(
                    "KVM exit: Internal error 0x%x\n",
                    run->internal.suberror);
            return 1;

        case KVM_EXIT_WATCHDOG:
            printf("KVM exit: Watchdog\n");
            return 1;

        case KVM_EXIT_SYSTEM_EVENT:
            printf(
                    "KVM exit: System event: 0x%x 0x%llx\n",
                    run->system_event.type,
                    run->system_event.flags);
            return run->system_event.type == KVM_SYSTEM_EVENT_SHUTDOWN ? 0 : 1;

        default:
            printf("KVM exit: Reason 0x%x\n", run->exit_reason);
            dump_regs(cpu.regs());
            dump_sregs(cpu.sregs());
            return 1;
        }
    }

    return 0;
}
