; SPDX-License-Identifier: GPL-2.0-or-later
; Copyright 2021 Tim Wiederhake

; Demo Boot Loader -- adapted for use in Virtual Machines
; Run: `loadvm --image demo_nasm_16:0x7c00 --arch x86`

; generate code for 16 bit mode
[bits 16]

; code will be located at 0000:7c00 == 0x0000 * 16 + 0x7c00.
[org 0x7c00]

; registers:
;       cs,ip:  0x0000:0x7c00
;       dl:     drive number for int 0x13
;       si:     partition number (vbr)
;       es,di   ptr to pnp support structure (maybe)
;       others: unspecified values

global _start
_start:
    ; setup stack
    mov     ax,     0x8000
    mov     sp,     ax
    mov     ax,     0x0000
    mov     bp,     ax
    mov     ss,     ax

    ; save important registers
    push    es                      ; pnp segment
    push    di                      ; pnp offset
    push    si                      ; partition number
    push    dx                      ; drive number

    ; copy code to 0x7a00
    mov     ds,     ax              ; src segment
    mov     es,     ax              ; dst segment
    mov     si,     0x7c00          ; src address
    mov     di,     0x7a00          ; dst address
    mov     cx,     0x0100          ; counter / number of two-byte words
    rep movsw                       ; copy!

    ; relocate
    jmp     0x0000:(0x7a00 + (.relocated - $$))
    .relocated:

    ; say hi
    mov     si,     msg_greeting
    call    print_string

    ; find partition flagged as active
    mov     ax,     0x0000
    cmp     byte [0x7bbe], 0x80
    je      .part1
    cmp     byte [0x7bce], 0x80
    je      .part2
    cmp     byte [0x7bde], 0x80
    je      .part3
    cmp     byte [0x7bee], 0x80
    je      .part4

    ; none found
    mov     si,     msg_no_active_partition
    call    print_string
    cli
    hlt
    jmp     $

.part4:
    inc     ax
.part3:
    inc     ax
.part2:
    inc     ax
.part1:
    inc     ax

    ; ... boot partition number ax (not shown)

; print the string in ds:si to the screen. clobbers si, ax and dx.
print_string:
    mov     dx,     0x03f8

.next_byte:
    ; al = [ds:si]; ++si;
    lodsb

    ; if (al == '\0') goto .done
    cmp     al,     0x00
    je      .done

    out     dx,     al

    ; next character
    jmp     .next_byte

    .done:
    ret

msg_greeting:
    db      'Demo Boot Loader v0.1', 10, 0

msg_no_active_partition:
    db      'Error: No active partition!', 10, 0

    ; fill the remaining code section with nops
    times   446 - ($ - $$) db 0x90

partition_table:
    ; fill the partition table entries with zeroes
    times   510 - ($ - $$) db 0x00

    ; add boot signature at the end of bootloader
    dw 0xAA55
