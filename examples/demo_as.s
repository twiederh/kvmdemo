// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

// Print first 20 Fibonacci numbers
// Run: `loadvm --image demo_as_32:0x1000 --arch i386`

.set    TARGET_VM,      1

.global _start
_start:
    // fibonacci();
    call fibonacci

    // exit(0);
.if TARGET_VM
    mov     $0x00,      %eax
    hlt
.else
    mov     $0x01,      %eax
    mov     $0x00,      %ebx
    int     $0x80
.endif


// void putchar(int c);
.global putchar
.align 16
putchar:
    push    %ebp
    mov     %esp,       %ebp

    push    %ebx

    // write(/*fd*/ 1, /*buf*/ &c, /*count*/ 1);
.if TARGET_VM
    mov     $0x03f8,    %edx
    mov     0x08(%ebp), %eax
    out     %al,        %edx
.else
    mov     $0x04,      %eax
    mov     $0x01,      %ebx
    lea     0x08(%ebp), %ecx
    mov     $0x01,      %edx
    int     $0x80
.endif

    pop     %ebx

    mov     %ebp,       %esp
    pop     %ebp
    ret



// void putdigit(int number);
.global putdigit
.align 16
putdigit:
    push    %ebp
    mov     %esp,       %ebp

    // %eax = number & 0x0f;
    mov     0x08(%ebp), %eax
    and     $0x0f,      %eax

    // if (%eax < $10) goto .is_digit;
    cmp     $0x0a,      %eax
    jl     .is_digit

    // %eax += 'a' - '0' - $10;
    add     $0x27,      %eax

.is_digit:
    // %eax += '0';
    add     $0x30,      %eax

    // putchar(%eax);
    push    %eax
    call    putchar
    add     $0x04,      %esp

    mov     %ebp,       %esp
    pop     %ebp
    ret



// void putnumber(int number);
.global putnumber
.align 16
putnumber:
    push    %ebp
    mov     %esp,       %ebp

    // putchar('0');
    push    $'0'
    call    putchar
    add     $0x04,      %esp

    // putchar('x');
    push    $'x'
    call    putchar
    add     $0x04,      %esp

    // %eax = number;
    mov     0x08(%ebp), %eax

    // push %eax, %eax >> 4, %eax >> 8, and %eax >> 12
    push    %eax
    sar     $0x04,      %eax
    push    %eax
    sar     $0x04,      %eax
    push    %eax
    sar     $0x04,      %eax
    push    %eax

    // putchar(%eax >> 12);
    call    putdigit
    add     $0x04,      %esp

    // putchar(%eax >> 8);
    call    putdigit
    add     $0x04,      %esp

    // putchar(%eax >> 4);
    call    putdigit
    add     $0x04,      %esp

    // putchar(%eax >> 0);
    call    putdigit
    add     $0x04,      %esp

    // putchar('\n');
    push    $'\n'
    call    putchar
    add     $0x04,      %esp

    mov     %ebp,       %esp
    pop     %ebp
    ret



// void fibonacci(void);
.global fibonacci
.align 16
fibonacci:
    push    %ebp
    mov     %esp,       %ebp

    // int i = 0;
    push    $0x00000000

    // int a = 1;
    push    $0x00000001

    // int b = 1;
    push    $0x00000001

    // putnumber(1);
    // putnumber(1);
    call    putnumber
    call    putnumber

.begin_for:
    // if (i >= 18) goto .end_for;
    mov     -4(%ebp),   %eax
    cmp     $18,        %eax
    jge     .end_for

    // i += 1;
    inc     %eax
    mov     %eax,       -4(%ebp)

    // %ecx = a + b;
    mov     -8(%ebp),   %ecx
    mov     -12(%ebp),  %eax
    add     %eax,       %ecx

    // a = b;
    mov     %eax,       -8(%ebp)

    // b = %ecx;
    mov     %ecx,       -12(%ebp)

    // putnumber(%ecx);
    push    %ecx
    call    putnumber
    add     $4,         %esp

    jmp .begin_for
.end_for:

    mov     %ebp,       %esp
    pop     %ebp
    ret
