// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

// "Hello World"
// Run: `loadvm --elf demo_c_32` / `loadvm --elf demo_c_64`

// This would go into some kind of "libguest" library

extern int main();

void outb(unsigned short port, char value) {
    asm("outb %0,%1"
        :
        : "a"(value), "Nd"(port)
        : "memory");
}

__attribute__((noreturn)) void exit(int status) {
    while (1)
        asm("hlt"
            :
            : "a"(status)
            : "memory");
}

int puts(const char* str) {
    int i = 0;

    while (*str) {
        outb(0x3f8, *str);
        str += 1;
    }

    return i;
}

__attribute__((noreturn)) void _start(void) {
    exit(main(1, ""));
}

// This is the actual example

int main() {
    puts("Hello World!\n");
    return 42;
}
