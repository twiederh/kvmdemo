// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "kvmmm.hpp"

#include <gtest/gtest.h>

#include <type_traits>

TEST(KVM, Construction) {
    EXPECT_TRUE(std::is_default_constructible_v<kvmmm::KVM>);
    EXPECT_FALSE(std::is_copy_constructible_v<kvmmm::KVM>);
    EXPECT_FALSE(std::is_copy_assignable_v<kvmmm::KVM>);
    EXPECT_TRUE(std::is_nothrow_move_constructible_v<kvmmm::KVM>);
    EXPECT_TRUE(std::is_nothrow_move_assignable_v<kvmmm::KVM>);
}

TEST(KVM, OpenDevice) {
    EXPECT_NO_THROW((void)kvmmm::KVM {});
    EXPECT_NO_THROW((void)kvmmm::KVM { -1 });
    EXPECT_THROW((void)kvmmm::KVM { "/dev/null" }, std::system_error);
}

TEST(VM, Construction) {
    EXPECT_FALSE(std::is_default_constructible_v<kvmmm::VM>);
    EXPECT_FALSE(std::is_copy_constructible_v<kvmmm::VM>);
    EXPECT_FALSE(std::is_copy_assignable_v<kvmmm::VM>);
    EXPECT_TRUE(std::is_nothrow_move_constructible_v<kvmmm::VM>);
    EXPECT_TRUE(std::is_nothrow_move_assignable_v<kvmmm::VM>);
}

TEST(CPU, Construction) {
    EXPECT_FALSE(std::is_default_constructible_v<kvmmm::CPU>);
    EXPECT_FALSE(std::is_copy_constructible_v<kvmmm::CPU>);
    EXPECT_FALSE(std::is_copy_assignable_v<kvmmm::CPU>);
    EXPECT_TRUE(std::is_nothrow_move_constructible_v<kvmmm::CPU>);
    EXPECT_TRUE(std::is_nothrow_move_assignable_v<kvmmm::CPU>);
}

TEST(Mem, Construction) {
    EXPECT_FALSE(std::is_default_constructible_v<kvmmm::Mem>);
    EXPECT_FALSE(std::is_copy_constructible_v<kvmmm::Mem>);
    EXPECT_FALSE(std::is_copy_assignable_v<kvmmm::Mem>);
    EXPECT_TRUE(std::is_nothrow_move_constructible_v<kvmmm::Mem>);
    EXPECT_TRUE(std::is_nothrow_move_assignable_v<kvmmm::Mem>);
}

int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
