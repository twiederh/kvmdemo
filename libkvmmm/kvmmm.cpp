// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#include "kvmmm.hpp"

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <system_error>

namespace kvmmm {

namespace {

class FileDescriptor {
public:
    [[nodiscard]] FileDescriptor(int fd) noexcept :
        m_fd { fd } {
    }

    ~FileDescriptor() {
        if (m_fd < 0) {
            return;
        }
        ::close(m_fd);
    }

    operator int() const {
        return m_fd;
    }

    int release() {
        int fd { -1 };
        std::swap(fd, m_fd);
        return fd;
    }

private:
    int m_fd { -1 };
};

#define open_or_throw(filename, flags) \
    [&](const char* location) { \
        int ret = ::open((filename), (flags)); \
        if (!(ret < 0)) { \
            return FileDescriptor { ret }; \
        } \
        auto message = std::string(location) \
                + ": open(" + filename + ", " #flags ") < 0"; \
        throw std::system_error { errno, std::system_category(), message }; \
    }(__PRETTY_FUNCTION__)

#define ioctl_or_throw(fd, arg1, arg2, throw_condition) \
    [&](const char* location) { \
        int ret = ::ioctl((fd), (arg1), (arg2)); \
        if (!(ret throw_condition)) { \
            return ret; \
        }; \
        auto message = std::string(location) \
                + ": ioctl(" #fd ", " #arg1 ", " #arg2 ") " #throw_condition; \
        throw std::system_error { errno, std::system_category(), message }; \
    }(__PRETTY_FUNCTION__)

#define mmap_or_throw(size, prot, flags, fd, throw_condition) \
    [&](const char* location) { \
        void* ret = ::mmap(nullptr, size, prot, flags, fd, 0); \
        if (!(ret throw_condition)) { \
            return ret; \
        }; \
        auto message = std::string(location) \
                + ": mmap(nullptr, " #size ", " #prot ", " #flags ", " #fd \
                  ", 0) " #throw_condition; \
        throw std::system_error { errno, std::system_category(), message }; \
    }(__PRETTY_FUNCTION__)

} /* namespace */

Mem Mem::memory(size_t size) {
    auto ptr = mmap_or_throw(size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, == MAP_FAILED);
    return Mem { ptr, size };
}

Mem Mem::file(std::string_view filename) {
    FileDescriptor fd = open_or_throw(filename.data(), O_RDONLY);
    struct ::stat stat;
    if (::fstat(fd, &stat) < 0 || stat.st_size < 0) {
        auto message = std::string(__PRETTY_FUNCTION__)
                + ": fstat(" + filename.data() + ") < 0";
        throw std::system_error { errno, std::system_category(), message };
    }
    size_t size = static_cast<size_t>(stat.st_size);
    auto ptr = mmap_or_throw(size, PROT_READ, MAP_PRIVATE, fd, == MAP_FAILED);
    return Mem { ptr, size };
}

Mem::~Mem() noexcept {
    if (m_ptr == MAP_FAILED) {
        return;
    }
    ::munmap(m_ptr, m_size);
}

void Mem::insert(size_t position, const void* buffer, size_t buffer_length) {
    for (size_t i = 0; i < buffer_length; ++i) {
        m_ptr[position + i] = static_cast<const std::byte*>(buffer)[i];
    }
}

void Mem::insert_file(size_t position, std::string_view filename) {
    Mem buffer = Mem::file(filename);
    insert(position, buffer.ptr(), buffer.size());
}

CPU::~CPU() noexcept {
    if (m_fd < 0) {
        return;
    }

    ::close(m_fd);
}

kvm_regs CPU::regs() const {
    kvm_regs retval;
    ioctl_or_throw(m_fd, KVM_GET_REGS, &retval, < 0);
    return retval;
}

void CPU::regs(const kvm_regs& data) {
    ioctl_or_throw(m_fd, KVM_SET_REGS, &data, < 0);
}

kvm_sregs CPU::sregs() const {
    kvm_sregs retval;
    ioctl_or_throw(m_fd, KVM_GET_SREGS, &retval, < 0);
    return retval;
}

void CPU::sregs(const kvm_sregs& data) {
    ioctl_or_throw(m_fd, KVM_SET_SREGS, &data, < 0);
}

void CPU::run() {
    ioctl_or_throw(m_fd, KVM_RUN, 0, < 0);
}

VM::~VM() noexcept {
    if (m_fd < 0) {
        return;
    }

    ::close(m_fd);
}

CPU VM::create_cpu() const {
    int fd = ioctl_or_throw(m_fd, KVM_CREATE_VCPU, 0, < 0);
    return CPU { fd };
}

void VM::set_memory(
        unsigned int slot,
        size_t guest_address,
        Mem& mem,
        bool readonly) {

    kvm_userspace_memory_region region {
        slot,
        static_cast<__u32>(readonly ? KVM_MEM_READONLY : 0),
        guest_address,
        mem.size(),
        reinterpret_cast<__u64>(mem.ptr())
    };

    ioctl_or_throw(m_fd, KVM_SET_USER_MEMORY_REGION, &region, < 0);
}

KVM::KVM(const std::string_view device) {
    FileDescriptor fd = open_or_throw(device.data(), O_RDWR);
    ioctl_or_throw(fd, KVM_GET_API_VERSION, 0, != KVM_API_VERSION);
    ioctl_or_throw(fd, KVM_CHECK_EXTENSION, KVM_CAP_CHECK_EXTENSION_VM, != 1);
    m_fd = fd.release();
}

KVM::~KVM() noexcept {
    if (m_fd < 0) {
        return;
    }

    ::close(m_fd);
}

VM KVM::create_vm() const {
    FileDescriptor fd = ioctl_or_throw(m_fd, KVM_CREATE_VM, 0, < 0);
    ioctl_or_throw(fd, KVM_CHECK_EXTENSION, KVM_CAP_USER_MEMORY, != 1);
    return VM { fd.release() };
}

Mem KVM::create_cpu_map(const CPU& cpu) const {
    int size = ioctl_or_throw(m_fd, KVM_GET_VCPU_MMAP_SIZE, 0, < 0);
    void* ptr = mmap_or_throw(size, PROT_READ | PROT_WRITE, MAP_SHARED, cpu.fd(), == MAP_FAILED);
    return Mem { ptr, static_cast<size_t>(size) };
}

void initialize_real_mode(CPU& cpu, __u64 entry) {
    kvm_regs regs {};
    regs.rflags = 2;
    regs.rip = entry;
    cpu.regs(regs);

    kvm_segment seg_code {
        0, /* base: selector << 4 */
        0xFFFF, /* limit: segment size: 64k */
        0x0000, /* selector: value visible from VM */
        0x0B, /* type: execute, readable, accessed */
        1, /* present: always true */
        0, /* dpl: descriptor privilege level: ring 0 */
        0, /* db: default operation size = 16 bit */
        1, /* s: descriptor type: non-system = code or data */
        0, /* l: reserved in non-64bit mode */
        0, /* g: byte granularity */
        0, /* avl: reserved for VM kernel use */
        0, /* unusable */
        0, /* padding */
    };

    kvm_segment seg_data {
        0, /* base: selector << 4 */
        0xFFFF, /* limit: segment size: 64k */
        0x0000, /* selector: value visible from VM */
        0x03, /* type: readable, writeable, accessed */
        1, /* present: always true */
        0, /* dpl: descriptor privilege level: ring 0 */
        0, /* db: default operation size = 16 bit */
        1, /* s: descriptor type: non-system = code or data */
        0, /* l: reserved in non-64bit mode */
        0, /* g: byte granularity */
        0, /* avl: reserved for VM kernel use */
        0, /* unusable */
        0, /* padding */
    };

    kvm_sregs sregs = cpu.sregs();
    sregs.cs = seg_code;
    sregs.ds = seg_data;
    sregs.es = seg_data;
    sregs.fs = seg_data;
    sregs.gs = seg_data;
    sregs.ss = seg_data;
    cpu.sregs(sregs);
}

void initialize_protected_mode(CPU& cpu, __u64 entry) {
    kvm_regs regs {};
    regs.rflags = 2;
    regs.rip = entry;
    cpu.regs(regs);

    kvm_segment seg_code {
        0, /* base: entire address space*/
        0xFFFFFFFF, /* limit: entire address space */
        0x08, /* selector: first */
        11, /* type: execute, read/write, accessed */
        1, /* present: yes */
        0, /* dpl: descriptor privilege level: ring 0 */
        1, /* db: default operation size = 32 bit */
        1, /* s: descriptor type: non-system = code or data */
        0, /* l: reserved in non-64bit mode */
        1, /* g: 4 KB granularity */
        0, /* avl: reserved for VM kernel use */
        0, /* unusable */
        0, /* padding */
    };

    kvm_segment seg_data {
        0, /* base: entire address space*/
        0xFFFFFFFF, /* limit: entire address space */
        0x10, /* selector: second */
        3, /* type: read/write, accessed */
        1, /* present: yes */
        0, /* dpl: descriptor privilege level: ring 0 */
        1, /* db: default operation size = 32 bit */
        1, /* s: descriptor type: non-system = code or data */
        0, /* l: reserved in non-64bit mode */
        1, /* g: 4 KB granularity */
        0, /* avl: reserved for VM kernel use */
        0, /* unusable */
        0, /* padding */
    };

    kvm_sregs sregs = cpu.sregs();
    sregs.cs = seg_code;
    sregs.ds = seg_data;
    sregs.es = seg_data;
    sregs.fs = seg_data;
    sregs.gs = seg_data;
    sregs.ss = seg_data;
    sregs.cr0 |= 0x01; /* protected mode */
    cpu.sregs(sregs);
}

void initialize_long_mode(CPU& cpu, Mem& ram, __u64 entry) {
    constexpr auto entries_per_page = 4096 / sizeof(uint64_t);
    constexpr size_t addr_pml4 = 0x1000;
    constexpr size_t addr_pdpt = 0x2000;

    /* present, writeable, usermode */
    ram.as<uint64_t>(addr_pml4) = 0x07 | addr_pdpt;

    for (size_t i = 1; i < entries_per_page; ++i) {
        ram.as<uint64_t>(addr_pml4 + i * sizeof(uint64_t)) = 0ULL;
    }

    /* present, writeable, usermode, 1GB/entry */
    for (size_t i = 0; i < 4; ++i) {
        uint64_t offset = i * 0x40000000;
        ram.as<uint64_t>(addr_pdpt + i * sizeof(uint64_t)) = 0x87 | offset;
    }
    for (size_t i = 4; i < entries_per_page; ++i) {
        ram.as<uint64_t>(addr_pdpt + i * sizeof(uint64_t)) = 0ULL;
    }

    struct kvm_regs regs {};
    regs.rflags = 2;
    regs.rip = entry;
    regs.rbp = 0x0000000100000000ULL;
    regs.rsp = 0x0000000100000000ULL;
    cpu.regs(regs);

    kvm_segment seg_code {
        0, /* base: entire address space*/
        0xFFFFFFFF, /* limit: entire address space */
        0x08, /* selector: first */
        11, /* type: execute, read/write, accessed */
        1, /* present: yes */
        0, /* dpl: descriptor privilege level: ring 0 */
        0, /* db: must be cleared if l == 1 */
        1, /* s: descriptor type: non-system = code or data */
        1, /* l: native 64 bit instructions */
        1, /* g: 4 KB granularity */
        0, /* avl: reserved for VM kernel use */
        0, /* unusable */
        0, /* padding */
    };

    kvm_segment seg_data {
        0, /* base: entire address space*/
        0xFFFFFFFF, /* limit: entire address space */
        0x10, /* selector: second */
        3, /* type: read/write, accessed */
        1, /* present: yes */
        0, /* dpl: descriptor privilege level: ring 0 */
        0, /* db: must be cleared if l == 1 */
        1, /* s: descriptor type: non-system = code or data */
        1, /* l: native 64 bit instructions */
        1, /* g: 4 KB granularity */
        0, /* avl: reserved for VM kernel use */
        0, /* unusable */
        0, /* padding */
    };

    kvm_sregs sregs = cpu.sregs();
    sregs.cs = seg_code;
    sregs.ds = seg_data;
    sregs.es = seg_data;
    sregs.fs = seg_data;
    sregs.gs = seg_data;
    sregs.ss = seg_data;
    sregs.cr0 |= 0x80000001; /* protected mode, paging */
    sregs.cr3 = addr_pml4;
    sregs.cr4 = 0x20; /* PAE paging */
    sregs.efer = 0x500; /* long mode enabled, long mode active */
    cpu.sregs(sregs);
}

} /* namespace kvmmm */
