// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2021 Tim Wiederhake

#ifndef KVMMM_HPP_
#define KVMMM_HPP_

// see https://www.kernel.org/doc/html/latest/virt/kvm/api.html
#include <linux/kvm.h>

#include <string_view>

namespace kvmmm {

enum {
    ENTRY_REAL_MODE = 0x0000000000007C00ULL,
    ENTRY_PROTECTED_MODE = 0x0000000008048000ULL,
    ENTRY_LONG_MODE = 0x0000000000400000ULL,
};

class Mem {
public:
    [[nodiscard]] static Mem memory(size_t size);

    [[nodiscard]] static Mem file(std::string_view filename);

    [[nodiscard]] Mem(void* ptr, size_t size) :
        m_ptr { static_cast<std::byte*>(ptr) },
        m_size { size } {
    }

    [[nodiscard]] Mem(const Mem& other) noexcept = delete;

    [[nodiscard]] Mem(Mem&& other) noexcept {
        std::swap(m_ptr, other.m_ptr);
        std::swap(m_size, other.m_size);
    }

    Mem& operator=(const Mem& other) noexcept = delete;

    Mem& operator=(Mem&& other) noexcept {
        std::swap(m_ptr, other.m_ptr);
        std::swap(m_size, other.m_size);
        return *this;
    }

    ~Mem() noexcept;

    [[nodiscard]] std::byte* ptr() noexcept {
        return m_ptr;
    }

    [[nodiscard]] const std::byte* ptr() const noexcept {
        return m_ptr;
    }

    [[nodiscard]] const size_t& size() const noexcept {
        return m_size;
    }

    template<typename T>
    [[nodiscard]] const T& as(size_t offset) const {
        return *reinterpret_cast<T*>(m_ptr + offset);
    }

    template<typename T>
    [[nodiscard]] T& as(size_t offset) {
        return *reinterpret_cast<T*>(m_ptr + offset);
    }

    void insert(size_t position, const void* buffer, size_t buffer_length);

    void insert_file(size_t position, std::string_view filename);

private:
    std::byte* m_ptr;
    size_t m_size;
};

class CPU {
public:
    [[nodiscard]] explicit CPU(int fd) :
        m_fd { fd } {
    }

    [[nodiscard]] CPU(const CPU& other) noexcept = delete;

    [[nodiscard]] CPU(CPU&& other) noexcept {
        std::swap(m_fd, other.m_fd);
    }

    CPU& operator=(const CPU& other) noexcept = delete;

    CPU& operator=(CPU&& other) noexcept {
        std::swap(m_fd, other.m_fd);
        return *this;
    }

    ~CPU() noexcept;

    [[nodiscard]] const int& fd() const noexcept {
        return m_fd;
    }

    [[nodiscard]] kvm_regs regs() const;

    void regs(const kvm_regs& data);

    // see Intel SDM Vol 3A Chapter 3.4.5 "Segment Descriptors"
    [[nodiscard]] kvm_sregs sregs() const;

    void sregs(const kvm_sregs& data);

    void run();

private:
    int m_fd { -1 };
};

class VM {
public:
    [[nodiscard]] explicit VM(int fd) noexcept :
        m_fd { fd } {
    }

    [[nodiscard]] VM(const VM& other) noexcept = delete;

    [[nodiscard]] VM(VM&& other) noexcept {
        std::swap(m_fd, other.m_fd);
    }

    VM& operator=(const VM& other) noexcept = delete;

    VM& operator=(VM&& other) noexcept {
        std::swap(m_fd, other.m_fd);
        return *this;
    }

    ~VM() noexcept;

    [[nodiscard]] const int& fd() const noexcept {
        return m_fd;
    }

    [[nodiscard]] CPU create_cpu() const;

    void set_memory(
            unsigned int slot,
            size_t guest_address,
            Mem& mem,
            bool readonly = false);

private:
    int m_fd { -1 };
};

class KVM {
public:
    [[nodiscard]] explicit KVM(const std::string_view device = "/dev/kvm");

    [[nodiscard]] explicit KVM(int fd) noexcept :
        m_fd { fd } {
    }

    [[nodiscard]] KVM(const KVM& other) noexcept = delete;

    [[nodiscard]] KVM(KVM&& other) noexcept {
        std::swap(m_fd, other.m_fd);
    }

    KVM& operator=(const KVM& other) noexcept = delete;

    KVM& operator=(KVM&& other) noexcept {
        std::swap(m_fd, other.m_fd);
        return *this;
    }

    ~KVM() noexcept;

    [[nodiscard]] const int& fd() const noexcept {
        return m_fd;
    }

    [[nodiscard]] VM create_vm() const;

    [[nodiscard]] Mem create_cpu_map(const CPU& cpu) const;

private:
    int m_fd { -1 };
};

void initialize_real_mode(CPU& cpu, __u64 entry = ENTRY_REAL_MODE);

void initialize_protected_mode(CPU& cpu, __u64 entry = ENTRY_PROTECTED_MODE);

void initialize_long_mode(CPU& cpu, Mem& ram, __u64 entry = ENTRY_LONG_MODE);

} /* namespace kvmmm */

#endif /* KVMMM_H_ */
