# TL/DR

This is "explorative software" aka. "toying around" with Linux' KVM API.

Instead of full-system emulation, this project aims to load and run a
statically linked ELF file and / or one or more flat binary files in a VM.

If you find a use for this, please let me know.

# kvmmm

kvmmm ("KVM minus minus") is a thin C++ wrapper for KVM's API.

Example usage:

```{.cpp}
#include "kvmmm.hpp"

#include <cstdio>

/* 16 bit code, output a '!' to port 42 */
constexpr unsigned char code[] = {
    0xB8, 0x21, 0x00,   /* mov $0x21, %ax */
    0xE7, 0x2A,         /* out %ax, $42 */
    0xF4                /* hlt */
};

int main() {
    kvmmm::KVM kvm {};

    kvmmm::Mem ram = kvmmm::Mem::memory(1 * 1024 * 1024);
    ram.insert(0, code, sizeof(code));

    kvmmm::VM vm = kvm.create_vm();
    vm.set_memory(0, 0, ram);

    kvmmm::CPU cpu = vm.create_cpu();
    kvmmm::initialize_real_mode(cpu, 0);

    kvmmm::Mem cpu_mmap = kvm.create_cpu_map(cpu);
    kvm_run& run = cpu_mmap.as<kvm_run>(0);
    while(true) {
        cpu.run();

        if (run.exit_reason == KVM_EXIT_IO && run.io.port == 42) {
            putchar(cpu_mmap.as<char>(run.io.data_offset));
            continue;
        }

        if (run.exit_reason == KVM_EXIT_HLT) {
            break;
        }

        return 1;
    }

    return 0;
}
```

## loadvm

Load data to run with KVM.

```
Usage: loadvm [OPTION...]

Options:
  -h, --help            Display this help
  -v, --verbose         Increase verbosity
  -e ADDRESS, --entry ADDRESS
                        Set entry point for execution
  -a ARCHITECTURE, --arch ARCHITECTURE
                        Set architecture
  --elf FILE            Load static executable
  --image FILE[:MEMORY_OFFSET[:FILE_OFFSET[:FILE_LENGTH]]]
                        Load code image into memory
```

* `ADDRESS` can be decimal or hex (prefixed with `0x`).
* `ARCHITECTURE` can be "x86" and "x86-16" for real mode; "i386" and "x86-32" 
  for protected mode; and "x86-64" and "amd64" for long mode.
* The entry address is automatically deduced for ELF files and defaults to the
  memory address of the first image.
* Only one ELF file, but multiple image files can be loaded.
* Image files that come later in the argument list may overwrite image files
  that come earlier in the argument list.

## Building and running

```{.sh}
$ meson build
$ ninja -C build
$ ninja -C test
```

## Examples

The `examples` directory contains three demos:
* Boot loader code, to be run in x86 real mode,
* Fibonacci sequence, to be run in x86 protected mode,
* Hello World, to be run in either protected or long mode.

```{.sh}
$ make -C examples
$ loadvm --elf examples/demo_c_32
Hello World!
```

## License

Copyright 2021 Tim Wiederhake
All code in this repository is licensed under the terms of GPL v2 or later.
